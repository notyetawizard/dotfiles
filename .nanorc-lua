##############################################################################
#
# Lua syntax highlighting for Nano.
#
# Author:  Matthew Wild <mwild1 (at) gmail.com>
# License: GPL 2  or later
#
# Version: 2007-06-06
#
# Notes: Originally based on Ruby syntax rc by Josef 'Jupp' Schugt
##############################################################################


# Automatically use for '.lua' files
syntax "Lua" ".*\.lua$"

# General
color white ".+"

# Operators
color brightwhite ":|\*\*|\*|/|%|\+|-|\^|>|>=|<|<=|~=|=|\.\.|\.\.\.|\<(not|and|or)\>"

# Statements
color cyan "\<(do|end|while|repeat|until|if|elseif|then|else|for|in|function|local|return)\>"

# Keywords
color magenta "\<(debug|string|math|table|io|coroutine|os|utf8|bit32)\>\."
color magenta "\<(_ENV|_G|_VERSION|assert|collectgarbage|dofile|error|getfenv|getmetatable|ipairs|load|loadfile|module|next|pairs|pcall|print|rawequal|rawget|rawlen|rawset|require|select|setfenv|setmetatable|tonumber|tostring|type|unpack|xpcall)\s*\("

# Standard library
color magenta "io\.\<(close|flush|input|lines|open|output|popen|read|tmpfile|type|write)\>"
color magenta "math\.\<(abs|acos|asin|atan2|atan|ceil|cosh|cos|deg|exp|floor|fmod|frexp|huge|ldexp|log10|log|max|maxinteger|min|mininteger|modf|pi|pow|rad|random|randomseed|sinh|sqrt|tan|tointeger|type|ult)\>"
color magenta "os\.\<(clock|date|difftime|execute|exit|getenv|remove|rename|setlocale|time|tmpname)\>"
color magenta "package\.\<(config|cpath|loaded|loadlib|path|preload|seeall|searchers|searchpath)\>"
color magenta "string\.\<(byte|char|dump|find|format|gmatch|gsub|len|lower|match|pack|packsize|rep|reverse|sub|unpack|upper)\>"
color magenta "table\.\<(concat|insert|maxn|move|pack|remove|sort|unpack)\>"
color magenta "utf8\.\<(char|charpattern|codes|codepoint|len|offset)\>"
color magenta "coroutine\.\<(create|isyieldable|resume|running|status|wrap|yield)\>"
color magenta "debug\.\<(debug|getfenv|gethook|getinfo|getlocal|getmetatable|getregistry|getupvalue|getuservalue|setfenv|sethook|setlocal|setmetatable|setupvalue|setuservalue|traceback|upvalueid|upvaluejoin)\>"
color magenta "bit32\.\<(arshift|band|bnot|bor|btest|bxor|extract|replace|lrotate|lshift|rrotate|rshift)\>"

# File handle methods
color magenta "\:\<(close|flush|lines|read|seek|setvbuf|write)\>"

# false, nil, true
color green "\<(false|nil|true)\>"

# External files
color red "(\<(dofile|require|include)|%q|%!|%Q|%r|%x)\>"

# Numbers
color green "\<([0-9]+)\>"

# Symbols
color magenta "(\(|\)|\[|\]|\{|\})"

# Strings
color green "\"(\\.|[^\\\"])*\"|'(\\.|[^\\'])*'"

# Multiline strings
color green start="\s*\[\[" end="\]\]"

# Escapes
color red "\\[0-7][0-7][0-7]|\\x[0-9a-fA-F][0-9a-fA-F]|\\[abefntrs]|(\\c|\\C-|\\M-|\\M-\\C-)."

# Shebang
color cyan "^#!.*"

# Simple comments
color yellow "\-\-.*$"

# Multiline comments
color yellow start="\s*\-\-\s*\[\[" end="\]\]"

# Trailing whitespaces
# color ,green "[[:space:]]+$"
