# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

PS1='[\u@\h \W]\$ '

export PAGER="less"
export EDITOR="nano"

#function random_hue
#{
#    color=$(($RANDOM % 7 + 31)) # set 31 to 30 for dark on light
#    PS1="\[\e[1;${color}m\]\\$ $reset"    # set 1 to 0 for dark on light
#}
#
#PROMPT_COMMAND="random_hue"
